#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//A variable
struct variable {
	char c; //Character
	char  *v;  //Value
	char *d; //Range of values
	size_t r;  //Number of values
};


/* A sum of vars -a half of a equation
 * i.e  part = ax + y
 * p[0] = x , m[0] = a
 * p[1] = y , m[1] = 1
 */
struct part {
	size_t *p;
	size_t *m;
	size_t size; //Number of vars
	size_t *x;
};

//A constrain - equation of two parts
struct constrain {
	struct part * half1;
	struct part * half2;
};



//Reverses a string
void rvrsString(char * string) {
	char t;
	size_t length = strlen(string) - 1;
	size_t i, j, s = (length / 2) ;
	if (length % 2 == 1)
		s++;

	for (i = 0, j = length; i < s; i++, j--) {
		t = string[j];
		string[j] = string[i];
		string[i] = t;
	}
}
//For base over 10
char numToChar(int n) {
	if (n >= 0 && n <= 9)
		return (char)(n + '0');
	else
		return (char)(n - 10 + 'A');
}

int charToNum(char c)
{
	if (c >= '0' && c <= '9')
		return (int) c - '0';
	else
		return (int) c - 'A' + 10;
}
//Converts a number from n-base to 10-base
size_t toDec(char * str, int base)
{
	size_t power = 1;
	size_t num = 0;
	if (charToNum(str[0]) >= base)
		return -1;
	num += charToNum(str[0]) * power;
	power = power * base;
	return num;
}
//Inserts a variable to a variable array if not already inserted
//Returns the index in the Variables array
size_t  insertVar(struct variable var, struct variable ** Variables, size_t * size, int overwrite) {

	// x vars have no character
	if (var.c != 0) {
		//Check if variable is already in array
		for (size_t i = 0; i < (*size); i++)
		{
			//If already inserted returns it's index
			if ((*Variables)[i].c == var.c) {
				if (overwrite == 1) {
					(*Variables)[i].r = var.r;
					free((*Variables)[i].d);
					(*Variables)[i].d = malloc(sizeof(char) * var.r);
					for (size_t y = 0; y < var.r; y++) {
						(*Variables)[i].d[y] = var.d[y];
					}
				}
				return i;
			}
		}
	}
	//If not already inserted

	//If not the the first inserted variable
	if ((*size) > 0)
		// Allocate memory for insertion
		(*Variables) = realloc((*Variables), ((*size) + 1) * sizeof(struct variable));
	//Store the variable
	//Character
	(*Variables)[(*size)].c = var.c;
	//Allocated memory for range of possible value
	(*Variables)[(*size)].d = malloc(sizeof(char) * var.r);
	//Number of possible values
	(*Variables)[(*size)].r = var.r;
	//Init value
	(*Variables)[(*size)].v = NULL;
	// strcpy((*Variables)[(*size)].v,var.v);

	//Copies the array of possible values
	for (size_t i = 0; i < var.r; i++)
	{
		(*Variables)[(*size)].d[i] = var.d[i];
	}
	//Increase Variables arrays size counter
	(*size)++;
	//Return index of the inserted value in the array
	return (*size) - 1;
}

//Checks if the parts of a constrain are equal
//Both parts must have all their variables assigned
size_t check_constrain(struct constrain con, struct variable * Variables, int base) {

	size_t half1 = 0 , half2 = 0 ;
	//Calculate the first half of equation
	for (size_t j = 0 ; j < con.half1->size; j++) {
		//If all variable are assigned ,calculation is possible
		if (Variables[con.half1->p[j]].v == NULL )
			//else return
			return 1;
		half1 += toDec(Variables[con.half1->p[j]].v, base) * con.half1->m[j];

	}

	//Calculate the second half of equation
	for (size_t j = 0 ; j < con.half2->size; j++) {
		//If all variable are assigned ,calculation is possible
		if (Variables[con.half2->p[j]].v == NULL )
			//else return
			return 1;
		half2 += toDec(Variables[con.half2->p[j]].v, base) * con.half2->m[j];

	}

	//If both parts had their all of their variables assigned with a value
	// and they are equal return that the constrain is satisfied
	if (half1 == half2)
		return 1;
	//Not equal
	return 0;
}


size_t check_all_diff(struct variable * Variables, size_t var_size) {

	//Check if two variables have the same value
	for (size_t i = 0; i < var_size; i++) {
		//For non x variable and only assigned ones
		if (Variables[i].c != 0 && Variables[i].v != NULL) {
			//With other non x variable and only assigned ones
			//If the have the same value return conflict
			for (size_t j = i + 1; j < var_size; j++) {
				if (Variables[j].c != 0 && Variables[j].v != NULL && (Variables[i].v[0] == Variables[j].v[0])) {
					return 0;
				}
			}
		}
	}
	//No conflict found
	return 1;
}

//Given an array of Variables and their values and a list of constrains
//Returns 1 if all constrains are satisfied  or 0 if any of them fails
size_t check_consistency(struct constrain * constrains, size_t cons_len,
                         struct variable * Variables, size_t var_size, int base) {

	//Check if all variables have different values(Only for not x variables)
	if (check_all_diff(Variables, var_size) == 0)
		return 0;
	//Check if any constrain fails
	for (size_t i = 0; i < cons_len; i++)
	{
		if (check_constrain(constrains[i], Variables, base) != 1)
			return 0;
	}
	//All is well
	return 1;
}
//Print results - a solution
void print_solution(struct variable * Variables, char * word1 , char * word2 , char * result,
                    size_t * word1IN, size_t * word2IN, size_t * resultIN, size_t solutions) {

	printf("\n%lu Solution found:\n", solutions);

	printf("   %s -> ", word1);
	for (int i = 0; i < strlen(word1); i++) {
		printf("%c", Variables[word1IN[strlen(word1) - i  - 1]].v[0]);
	}
	printf("\n   +\n   %s -> ", word2);
	for (int i = 0; i < strlen(word2); i++) {
		printf("%c", Variables[word2IN[strlen(word2) - 1 - i]].v[0]);
	}
	printf("\n   =\n   %s -> ", result);

	for (int i = 0; i < strlen(result); i++) {
		printf("%c", Variables[resultIN[strlen(result) - 1 - i]].v[0]);
	}
	printf("\n");

}

/* Recursive backtracking search
 * Given an array of Variables and a list of constrains tries to find a possible "solution"
 * Solution is when each variable is assigned a value from it's range of possible values
 * and all of the constrains are satisfied
 */
size_t recursive_backtracking( struct variable ** Variables, size_t first, size_t last,
                               struct constrain * constrains, size_t cons_len, int base,
                               char * word1 , char * word2 , char * resultw, size_t * word1IN,
                               size_t * word2IN, size_t * resultIN, size_t * solutions) {

	//Find next unassigned variable
	size_t i, result, var = 0;
	for (i = first; i < last; i++) {
		if ((*Variables)[i].v == NULL) {
			var = i;
			i = last + 2;
		}
	}
	//All assigned success!
	if (i == last) {
		(*solutions) = (*solutions) + 1;
		print_solution((*Variables), word1, word2, resultw, word1IN, word2IN, resultIN, (*solutions));
		return 0;
		// return 1;
	}
	//For each possible value in range of values of variable
	for (i = 0; i < (*Variables)[var].r; i++) {
		//temporary assign value
		(*Variables)[var].v = malloc(1);
		(*Variables)[var].v[0] = (*Variables)[var].d[i];

		//check if a the value is consisted with the constrains
		if (check_consistency(constrains, cons_len, (*Variables), last, base)) {
			//If no conflict found move to next unassigned variable
			result = recursive_backtracking(Variables, var + 1, last, constrains, cons_len, base, word1, word2, resultw, word1IN, word2IN, resultIN, solutions);
			if (result == 1)
				return 1;
		}
		//Conflict found ,unassigns value
		free((*Variables)[var].v);
		(*Variables)[var].v = NULL;


	}
	free((*Variables)[var].v);
	(*Variables)[var].v = NULL;
	return 0;
}
/* Inits the recursive backtracking search
 * returns 1 for success (found a solution)
 * returns 0 for fail (no solution possible)
 */
size_t backtracking_search( struct variable ** Variables, size_t var_size,
                            struct constrain * constrains, size_t cons_len, int base,
                            char * word1 , char * word2 , char * result, size_t * word1IN,
                            size_t * word2IN, size_t * resultIN, size_t * solutions) {

	return recursive_backtracking(Variables, 0, var_size, constrains, cons_len, base, word1, word2, result, word1IN, word2IN, resultIN, solutions);
}




size_t  getInput(char ** word1, char ** word2, char ** result, int * zero_start) {

	size_t max_len = 100, len = max_len;
	size_t w1_len  = 0, w2_len = 0, r_len = 0, base_len = 0;
	char * temp = malloc(max_len);
	int c;

	if (temp == NULL)
		return 0;
	printf("Allow zeros at start(y/n) ");
	c = fgetc(stdin);
	if (c == 'y')
		*zero_start = 1;
	else if (c == 'n')
		*zero_start = 0;
	else
		return 0;
	while (1) {
		c = fgetc(stdin);
		if (c == EOF ||  c == '\n')
			break;
	}

	printf("Enter number system: ");
	while (1) {
		c = fgetc(stdin);
		if (c == EOF ||  c == '\n')
			break;
		if (--len == 1) {
			len = max_len;
			char * temp = realloc(temp, max_len *= 2);
		}
		temp[base_len++] = c;
	}
	temp[base_len++] = '\0';

	*word1 = malloc(sizeof(char ) * w1_len);
	size_t base = atoi(temp);

	len = max_len;

	free(temp);
	temp = malloc(max_len);

	printf("Enter problem: ");
	//Read first word
	while (1) {
		c = fgetc(stdin);
		if (c == EOF ||  c == '\n')
			return 0;
		if (c == ' ' || c == '+' )
			break;
		if (--len == 1) {
			len = max_len;
			char * temp = realloc(temp, max_len *= 2);
		}
		temp[w1_len++] = c;
	}
	temp[w1_len++] = '\0';

	*word1 = malloc(sizeof(char ) * w1_len);
	strcpy((*word1), temp);

	len = max_len;
	free(temp);
	temp = malloc(max_len);

	while (1) {
		c = fgetc(stdin);
		if (c == EOF ||  c == '\n')
			return 0;
		if (c != ' ' &&  c != '+')
			break;
	}
	//Read second word
	while (1) {
		if (w2_len != 0)
			c = fgetc(stdin);
		if (c == EOF ||  c == '\n')
			return 0;
		if (c == ' ' || c == '+' )
			break;
		if (--len == 1) {
			len = max_len;
			temp = realloc(temp, max_len *= 2);
		}
		temp[w2_len++] = c;
	}
	temp[w2_len++] = '\0';
	(*word2) = malloc(sizeof(char) * w2_len);
	strcpy((*word2), temp);
	len = max_len;
	free(temp);
	temp = malloc(max_len);

	while (1) {
		c = fgetc(stdin);
		if (c == EOF ||  c == '\n')
			return 0;
		if (c != ' ' &&  c != '=')
			break;
	}

	//Read third word
	while (1) {
		if (r_len != 0)
			c = fgetc(stdin);
		if (c == EOF || c == ' ' ||  c == '\n' )
			break;
		if (--len == 1) {
			len = max_len;
			temp = realloc(temp, max_len *= 2);
		}
		temp[r_len++] = c;
	}

	temp[r_len++] = '\0';
	(*result) = malloc(sizeof(char) * strlen(temp));
	strcpy((*result), temp);
	free(temp);

	return base;
}



size_t main(size_t argc, char **argv) {

	char * word1, * word2, * result;
	int  * zero_start = malloc(sizeof(int));

//Read input
	size_t base =   getInput(&word1, &word2, &result, zero_start);
	if (base == 0) {
		printf("Error in input\n");
		exit(1);
	}
//reverses read strings
	rvrsString(word1); rvrsString(word2);
	rvrsString(result);

	/*
	 * Find if result have more letters
	 * than the added words
	 *  set  overflow  = 1
	 */
	size_t bigger_len = strlen(word1);
	if (bigger_len < strlen(word2))
		bigger_len = strlen(word2);
	int over;
	if (strlen(result) == bigger_len)
		over = 0;
	else if (strlen(result) > bigger_len)
		over = 1;
	else
		over = 0;

//Creates range of possible values for variables
// based on n-base , for binary 0,1 for dec 0,1,2..,9
	char * baseD = malloc(sizeof(char) * base);
	for (size_t i = 0; i < base; i++) {
		baseD[i] = numToChar(i);
	}

//Variables array init
	struct variable * Variables = malloc(sizeof(struct variable ));
	size_t *vars_size = malloc(sizeof(size_t));
	*vars_size = 0;

	size_t length  = strlen(result) - 1;
	struct variable t_var;

//X vars init based on result word length , they are used in constrains
	char * x_range = malloc(sizeof(char) * 2);
// possible values 1 or 0
	x_range[0] = numToChar(0); x_range[1] = numToChar(1);
	size_t * x_vars_size; size_t * x_varsIN;
	if (length > 0) {
		x_vars_size = malloc(sizeof(size_t));
		*x_vars_size = length - 1;
		x_varsIN = malloc(sizeof(size_t) * (*x_vars_size));
//creates result string length - 1 vars
		for (size_t i = 0; i < length ; i++) {
			t_var.c = 0;
			t_var.d = x_range;
			t_var.r = 2;
			x_varsIN[i] = insertVar(t_var, &Variables, vars_size, 0 );
		}
	}

	t_var.c = word1[0];
	t_var.d = baseD;

	int overwrite = 0;
	size_t * word1IN = malloc(sizeof(size_t) * strlen(word1));
	size_t * word2IN = malloc(sizeof(size_t) * strlen(word2));
	size_t * resultIN = malloc(sizeof(size_t) * strlen(result));

//Insert variables from first word letters
	for (size_t i = 0; i < strlen(word1); i++) {
		t_var.c = word1[i];
		t_var.d = baseD;

		if (i <= length) {
			t_var.r = base;
			overwrite = 0;
		}
		else {
			t_var.r = 1;
			overwrite = 1;
		}
		if (i == strlen(word1) - 1 && (*zero_start) == 0) {
			t_var.d = &baseD[1];
			t_var.r--;
		}
		word1IN[i] = insertVar(t_var, &Variables, vars_size, overwrite);
	}
//Insert variables from second word letters
	for (size_t i = 0; i < strlen(word2); i++) {
		t_var.c = word2[i];
		if (i <= length) {
			t_var.r = base;
			overwrite = 0;
		}
		else {
			t_var.r = 1;
			overwrite = 1;
		}
		if (i == strlen(word2) - 1 && (*zero_start) == 0) {
			t_var.d = &baseD[1];
			t_var.r--;
		}
		word2IN[i] = insertVar(t_var, &Variables, vars_size, overwrite);
	}
//Insert variables from result word letters
	for (size_t i = 0; i < strlen(result); i++) {
		t_var.c = result[i];
		if (i < strlen(result) - 1 || over == 0 || length == 0) {
			t_var.d = &baseD[0];
			t_var.r = base;
			overwrite = 0;
		}
		else {
			t_var.d = &x_range[1];
			t_var.r = 1;
			overwrite = 1;
		}
		resultIN[i] = insertVar(t_var, &Variables, vars_size, overwrite);
	}

//Calculate constrains based on input

	struct constrain *constrains;
	constrains = malloc(strlen(result) * sizeof(struct constrain ));
//For string length of result
	for (size_t i = 0; i < length + 1 ; i++)
	{
		//Memory allocations
		constrains[i].half1 = malloc(sizeof(struct part ));
		constrains[i].half2 = malloc(sizeof(struct part ));


		size_t y = 3, k = 3, p = 2, z = 2;
		//Cases where first word is smaller than second one
		if (i >= strlen(word1)) {
			y--;
			p--;
		}
		//Cases where second word is smaller than the first one
		if (i >= strlen(word2)) {
			y--;
			k--;
		}
		//First constrain wont have a x var in first part
		if (i == 0)
			y = 2;
		//Last constrain wont have a x var in second part
		if (i == length) {
			z = 1;
			if (over == 1)
				y = 1;
		}
		constrains[i].half1->p =  malloc(sizeof(size_t) * y);
		constrains[i].half1->m =  malloc(sizeof(size_t) * y);
		constrains[i].half1->x =  malloc(sizeof(size_t) * y);

		constrains[i].half2->p =  malloc(sizeof(size_t) * z);
		constrains[i].half2->m =  malloc(sizeof(size_t) * z);
		constrains[i].half2->x =  malloc(sizeof(size_t) * z);
		constrains[i].half1->size = y ;
		constrains[i].half2->size = z ;

		if (i == 0) {
			y = 3;
			if (i >= strlen(word1))
				y--;
			if (i >= strlen(word2))
				y--;
		}
		if (y > 1) {
			if (i < strlen(word1)) {
				constrains[i].half1->p[y - k] =  word1IN[i];
				constrains[i].half1->m[y - k] =  1;
				constrains[i].half1->x[y - k] = 0;
			}
			if (i < strlen(word2)) {
				constrains[i].half1->p[y - p] =  word2IN[i];
				constrains[i].half1->m[y - p] =  1;
				constrains[i].half1->x[y - p] = 0;
			}

		}
		if (i > 0 && length > 0) {
			constrains[i].half1->p[y - (k - p)] =  x_varsIN[i - 1];
			constrains[i].half1->m[y - (k - p)] =  1;
			constrains[i].half1->x[y - (k - p)] = 1;
		}

		if (z > 1 && length > 0) {
			constrains[i].half2->p[z - 2] =  x_varsIN[i];
			constrains[i].half2->m[z - 2] =  base;
			constrains[i].half2->x[z - 2] = 1;
		}
		constrains[i].half2->p[z - 1] =  resultIN[i];
		constrains[i].half2->m[z - 1] =  1;
		constrains[i].half2->x[z - 1] = 0;
	}
//Prints calculated  constrains
	printf("\nConstrains:\n");
	for (size_t j = 0; j < length + 1; j++) {
		for (size_t i = 0; i < constrains[j].half1->size; i++) {
			if (constrains[j].half1->x[i] == 0)
				printf("%c ", 	Variables[constrains[j].half1->p[i]].c);
			else
				printf("X%lu ", j);
			if (i < constrains[j].half1->size - 1)
				printf("+ ");
		}
		printf("= " );

		for (size_t i = 0; i < constrains[j].half2->size; i++) {
			if (constrains[j].half2->x[i] == 0)
				printf("%c ", 	Variables[constrains[j].half2->p[i]].c);
			else
				printf("%d x X%lu ", 10, j + 1);
			if (i < constrains[j].half2->size - 1)
				printf("+ ");
		}
		printf("\n");
	}
	printf("\n");

	rvrsString(word1); rvrsString(word2);
	rvrsString(result);
	size_t *  solutions = malloc(sizeof(size_t));
	*solutions = 0;
//Start search
	backtracking_search(&Variables, (*vars_size),
	                    constrains, length + 1, base, word1, word2, result, word1IN, word2IN, resultIN, solutions);

	if ((*solutions) == 0)
		printf("No solutions found!\n");

	return 0;
}
