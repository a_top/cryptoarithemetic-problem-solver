# Cryptoarithemetic problem solver

CSP symbol to number solver with n-base support in C




## To run
On GNU/Linux:

1. Download csp.c and makefile

2. Compile with `$ make`

3. Run with `$ ./csp`

Alternative compile and run with `$ make run`

#
## Input format:

  1. Enter y for yes or n for no if you want zeros at start of words

  2. Enter number system ,i.e. 10 for 10-base

  3. Enter problem in this format WORD_1 + WORD2 = RESULT_WORD

  - Spaces are necessary  i.e. SEND + MORE = MONEY and not SEND+MORE=MONEY

#
### Example input:

./csp

Allow zeros at start(y/n) y

Enter number system: 6

Enter problem: TO + TO = FOR

