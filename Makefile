CC = gcc
DBUG = -g
CCFLAGS = -O2 -Wall -pedantic
OBJFILES = csp.o

TARGET = csp


all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) $(CFLAGS) $(DBG) -o $(TARGET) $(OBJFILES) -lm
run: $(TARGET)
	./${TARGET}
clean:
	rm -f $(TARGET) *.o
